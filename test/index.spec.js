import assert from "assert";
import simpleMock from "simple-mock";
import { LoudMap } from "../src/index.js";

describe("LoudMap", () => {
  describe("static from", () => {
    it("creates a LoudMap from a regular Map", () => {
      const map = new Map();
      map.set("a", 0);
      const loudMap = LoudMap.from(map);
      assert(loudMap instanceof LoudMap);
      assert.strictEqual(loudMap.get("a"), 0);
    });
  });
  describe("get", () => {
    it("returns a stored value", () => {
      const map = new LoudMap();
      map.set("a", 0);
      assert.strictEqual(map.get("a"), 0);
    });
    it("computes a computed value if not in cache", () => {
      const map = new LoudMap();
      map.compute("b", ["a"], (a) => a + 1);
      map.set("a", 0);
      assert.strictEqual(map.get("b"), 1);
    });
  });
  describe("set", () => {
    it("sets a value in the Map", () => {
      const map = new LoudMap();
      map.set("a", 0);
      assert.strictEqual(map.get("a"), 0);
    });
    it("deletes the key from map if value is undefined", () => {
      const map = new LoudMap();
      map.set("a", 0);
      map.set("a", void 0);
      const iterator = map.keys();
      assert.strictEqual(typeof iterator.next().value, "undefined");
    });
    it("emits a key-specific change event", (done) => {
      const map = new LoudMap();
      map.set("a", 0);
      map.events.on("change/a", (val, oldVal) => {
        assert.strictEqual(val, 1);
        assert.strictEqual(oldVal, 0);
        done();
      });
      map.set("a", 1);
    });
    it("emits a a global change event if isolated arg is true", (done) => {
      const map = new LoudMap();
      map.events.on("change", (changes) => {
        assert.strictEqual(changes.a, 0);
        done();
      });
      map.set("a", 0, true);
    });
  });
  describe("delete", () => {
    it("calls set method with undefined value", () => {
      const map = new LoudMap();
      simpleMock.mock(map, "set");
      map.delete("a");
      assert.deepEqual(map.set.lastCall.args, ["a", void 0, true]);
    });
  });
  describe("batch", () => {
    it("sets multiple values at the same time", () => {
      const map = new LoudMap();
      map.batch({
        a: 0,
        b: 1,
      });
      assert.strictEqual(map.get("a"), 0);
      assert.strictEqual(map.get("b"), 1);
    });
  });
  describe("compute", () => {
    it("adds a computed property", async () => {
      const map = new LoudMap();
      map.compute("b", ["a"], (a) => a + 1);
      map.compute("c", ["b"], (b) => Promise.resolve(b + 1));
      map.set("a", 5);
      assert.strictEqual(map.get("b"), 6);
      assert.strictEqual(await map.get("c"), 7);
      // duplicated on purpose, to test cached promise
      assert.strictEqual(await map.get("c"), 7);
    });
  });
});
