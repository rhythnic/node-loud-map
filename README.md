# LoudMap

**NOTICE**
This repo has relocated to [loud-map](https://gitlab.com/rhythnic/loud-map).

LoudMap extends the JS Map class to support state management.  LoudMap supports computed properties,
and it has an EventEmitter for notifying the user of state changes.

## Install

```
npm install loud-map
```

## Usage
LoudMap inherits all of the methods from [Map][map].  It also adds methods `batch`, `compute`, and `from`.

```
import { LoudMap } from 'loud-map'

const state = new LoudMap()

state.compute('menuClosed', ['menuOpen'], menuOpen => !menuOpen)

state.compute('user', ['userId'], async (userId) => {
  if (!userId) return null
  return fetch(`/users/${userId}`).then(res => res.json())
})

state.events.on('change/menuOpen', (value, oldValue) => {
  // ...
})

state.events.on('change', (changes) => {
  // changes is an object with all changed key/value pairs
})

state.batch({
  userId: '',
  menuOpen: false
})

// state.get('menuOpen') === false
state.set('menuOpen', true)
```

## API

### batch(values)
- values [Object][object]

Set many values at once.  An `change/KEY` is emitted for each key in values.
One `change` event is emitted at the end, with the values object as the payload.
You can delete a key via `batch` by passing `undefined` or `void 0` as the value.

### compute(key, dependencyKeys, computeHandler)
- key [String][string] - key at which to cache computed value in the map
- dependencyKeys [Array][array] - map keys on which this computed value depends
- computeHandler [Function][function] - function to compute a value

The computeHandler will receive all of the values of it's dependencyKeys as arguments.
A computeHandler can return a [Promise][promise].  In this case, the promise is cached.
Computed properties are lazy.  The computeFunction doesn't run until it is accessed with `get`.
The result is cached in the map, so future access won't trigger recomputation.
If the value of a dependencyKey changes, the computed value is deleted from cache and
computed again next time it is accessed.

### from(map)
- map [Map][map]

Convert a regular Map into a LoudMap

## Events
- If the user calls the `set` method, two events are emitted, `change/KEY` and `change`.
- If the user calls the batch method, one `change/KEY` event is emitted for each key, and
one `change` event is emitted at the end.
- Calling `delete` will emit the same events as `set`

## Contributing
Please use issues for all bug reports and feature requests.

[map]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map
[object]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object
[string]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String 
[function]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function
[array]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array
[promise]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
[boolean]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean
