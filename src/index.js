import EventEmitter from "events";

export class LoudMap extends Map {
  constructor() {
    super();
    this._eventSource = new EventEmitter();
    this._computed = new Map();
    this._inverseComputedDependencies = new Map();
  }
  static from(map) {
    let result = new LoudMap();
    map.forEach((val, key) => {
      result.set(key, val, false);
    });
    return result;
  }
  get events() {
    return this._eventSource;
  }
  get(key) {
    let val = super.get(key);
    if (typeof val === "undefined" && this._computed.has(key)) {
      return cacheComputedValue.call(this, key);
    }
    return val;
  }
  set(key, value, emitGlobalChange = true) {
    const oldValue = super.get(key);
    if (typeof value !== "undefined") super.set(key, value);
    else super.delete(key);
    let inverseDeps = this._inverseComputedDependencies.get(key);
    if (inverseDeps) inverseDeps.forEach((x) => this.delete(x, false));
    this.events.emit(`change/${key}`, value, oldValue);
    if (emitGlobalChange) {
      this.events.emit("change", { [key]: value });
    }
  }
  delete(key, emitGlobalChange = true) {
    return this.set(key, void 0, emitGlobalChange);
  }
  batch(changes) {
    const keys = Object.keys(changes);
    keys.forEach((key) => {
      this.set(key, changes[key], false);
    });
    this.events.emit("change", changes);
  }
  compute(key, deps, fn) {
    this._computed.set(key, { deps, fn });
    deps.forEach((dep) => {
      const computedKeys = this._inverseComputedDependencies.get(dep) || [];
      this._inverseComputedDependencies.set(dep, [...computedKeys, key]);
    });
  }
}

export function cacheComputedValue(key) {
  const computed = this._computed.get(key);
  if (!computed) {
    throw new Error(
      `Attempted to compute cache for unknown computed key ${key}`
    );
  }
  let val = computed.fn(...computed.deps.map((x) => this.get(x)));
  this.set(key, val, false);
  return val;
}
